import {getClicksPerTextMatchRepository} from '../repository/UserRepository'

export const getClicksPerTextMatch = (text:string) =>{
    return getClicksPerTextMatchRepository(text)
}