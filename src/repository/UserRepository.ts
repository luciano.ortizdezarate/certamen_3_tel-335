import User from "../model/User"
import {IUser} from '../model/User'

export const getUsersRepository = async ()=>{
    const users = await User.find()
    if (!users) {
        return {
            message: 'Internal server error'
        }
    }
    return {
        users
    }
}

export const getClicksPerTextMatchRepository = async (text:string) =>{

    const users = await getUsersRepository()
    let data:IUser[] = []

    users.users.forEach((user:IUser)=> {
        if(user.name.toLowerCase().search(text.toLowerCase()) != -1){
            data.push(user)
        }
    })
    if(!data){
        return{
            message:'There was no coincidences'
        }
    }
    data.sort((a:IUser,b:IUser) => b.times - a.times )
    return {data:data}
}

