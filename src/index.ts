import express from 'express'
import {getClicksPerTextMatch} from './controller/UserController'
import {connect} from './database'
const PORT = 4000       //Puerto en que corre la App
const app = express()

//Conexión con la base de datos
connect()

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))
//Endpoint para verificar estado de la App
app.get('/', (req, res) =>{
    res.json({message:'Estado de la App: OK'})
})

app.get('/clicks/user/:name', async (req, res) => {
    const response = await getClicksPerTextMatch(req.params.name)
    res.json(response)
})

app.listen(PORT, ()=>{
    console.log('App corriendo en puerto: ', PORT)
})