import mongoose from 'mongoose'

let database: mongoose.Connection
export const connect = ()=>{
    const uri = "mongodb+srv://typeahead:tETFarE84nqwZPTX@typeaheadcluster-lhren.mongodb.net/test?retryWrites=true&w=majority"
    if(database) return;

    mongoose.connect(uri,{
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })

    database = mongoose.connection

    database.once('open', async()=>{
        console.log("Conexión establecida con la BD")
    })

    database.on('error', ()=>{
        console.log("Error al conectar con la BD")
    })
}

export const disconnect = () => {
    if(!database) return
    mongoose.disconnect()
}