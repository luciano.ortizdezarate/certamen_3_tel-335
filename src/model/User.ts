import { Schema, model } from 'mongoose'

export interface IUser extends Document{
    name:string,
    times:number
}

const userSchema = new Schema({
    name: {
        type: String
    },
    times: {
        type: Number
    }
})

export default model('User', userSchema)